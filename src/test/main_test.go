package main

import (
	"flag"
	"log"
	"net/http/httptest"
	"os"
	"testing"

	"bitbucket.org/cescoferraro/api/src/backend/cats"
	"bitbucket.org/cescoferraro/api/src/backend/cmd"
	"bitbucket.org/cescoferraro/api/src/backend/iot"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
)

func TestMain(m *testing.M) {
	setup()
	exitVal := m.Run()
	teardown()
	os.Exit(exitVal)
}

func setup() {
	color.Cyan("[TESTS] - Bootstraping tests")
	flag.StringVar(&util.ServerFlag, "server", "s", "server to set request for")
	flag.Parse()
	if util.ServerFlag != "dev" {
		util.BasicStart()
		iot.RunDependencies()
		cats.ListS3()
		router := cmd.InitRootRouter()
		util.Server = httptest.NewServer(router)
	}
}

func teardown() {
	log.Println("[TESTS] - Teardown completed")

}
