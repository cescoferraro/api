package main

import (
	"net/http"
	"testing"

	"bitbucket.org/cescoferraro/api/src/backend/util"
)

func TestCats(t *testing.T) {

	var CatTests []util.TableTests
	CatTests = append(CatTests, util.TableTests{
		Method:      "GET",
		Path:        "/cats",
		Status:      http.StatusOK,
		Name:        "Get kitty pic",
		Description: "Should return a photo from my cats",
	})

	util.SpinTableTests(util.Server, t, CatTests)
}
