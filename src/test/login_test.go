package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"bitbucket.org/cescoferraro/api/src/backend/iot/user"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	randomdata "github.com/Pallinder/go-randomdata"
	"github.com/fatih/color"
	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"
)

var (
	UserID           = bson.NewObjectId()
	OriginalUserName = randomdata.SillyName()
	UpdatedUserName  = randomdata.SillyName()

	MockUser = user.User{
		ID:       UserID,
		Username: OriginalUserName,
		Password: "password",
		Email:    randomdata.Email(),
	}
)

func TestRegisterUser(t *testing.T) {
	r, err := http.NewRequest("POST", util.Server.URL+"/iot/user/register", SerializeUser(MockUser))
	assert.NoError(t, err)
	response, err := http.DefaultClient.Do(r)
	assert.NoError(t, err)
	actualBody, err := ioutil.ReadAll(response.Body)
	assert.NoError(t, err)
	util.LogIfVerbose(color.FgRed, "TEST", string(actualBody))
}

func TestLoginUser(t *testing.T) {
	r, err := http.NewRequest("POST", util.Server.URL+"/iot/user/login", SerializeUser(MockUser))
	assert.NoError(t, err)
	response, err := http.DefaultClient.Do(r)
	assert.NoError(t, err)
	actualBody, err := ioutil.ReadAll(response.Body)
	assert.NoError(t, err)
	util.LogIfVerbose(color.FgRed, "TEST", string(actualBody))
}

func SerializeUser(User user.User) io.Reader {
	b, _ := json.Marshal(User)
	return strings.NewReader(string(b))
}
