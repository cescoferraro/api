package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"

	"bitbucket.org/cescoferraro/api/src/backend/iot/house"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	randomdata "github.com/Pallinder/go-randomdata"
	"gopkg.in/mgo.v2/bson"
)

var (
	HouseID           = bson.NewObjectId()
	OriginalHouseName = randomdata.SillyName()
	UpdatedHouseName  = randomdata.SillyName()

	MockHouse = house.House{
		ID:   HouseID,
		Name: OriginalHouseName}

	UpdateMockHouse = house.House{
		ID:   HouseID,
		Name: UpdatedHouseName}
)

func TestHouse(t *testing.T) {
	var HouseTests []util.TableTests
	HouseTests = append(HouseTests, util.TableTests{
		Method:       "POST",
		Path:         "/iot/house/create",
		Body:         SerializeHouse(MockHouse),
		BodyContains: MockHouse.Name,
		Status:       http.StatusOK,
		Name:         "CreateHouse",
		Description:  "Creates a house object",
	}, util.TableTests{
		Method:       "POST",
		Path:         "/iot/house/update",
		Body:         SerializeHouse(UpdateMockHouse),
		BodyContains: "House Updated",
		Status:       http.StatusOK,
		Name:         "UpdateHouse",
		Description:  "Update a house object",
	}, util.TableTests{
		Method:       "POST",
		Path:         "/iot/house/read",
		Body:         SerializeHouse(UpdateMockHouse),
		BodyContains: UpdateMockHouse.Name,
		Status:       http.StatusOK,
		Name:         "ReadHouse",
		Description:  "Read a house object",
	}, util.TableTests{
		Method:       "POST",
		Path:         "/iot/house/delete",
		Body:         SerializeHouse(UpdateMockHouse),
		BodyContains: "House Deleted",
		Status:       http.StatusOK,
		Name:         "DeleteHouse",
		Description:  "Delete a house object",
	})

	util.SpinTableTests(util.Server, t, HouseTests)
}

func SerializeHouse(house house.House) io.Reader {
	b, _ := json.Marshal(house)
	return strings.NewReader(string(b))
}
