package main

import (
	"net/http"
	"testing"

	"bitbucket.org/cescoferraro/api/src/backend/util"
)

func TestRoot(t *testing.T) {

	var AllTests []util.TableTests
	AllTests = append(AllTests, util.TableTests{
		Method:      "GET",
		Path:        "/",
		Status:      http.StatusOK,
		Name:        "NothingAtRoot",
		Description: "Root path should return 200",
	})

	util.SpinTableTests(util.Server, t, AllTests)
}
