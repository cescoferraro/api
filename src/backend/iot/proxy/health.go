package proxy

import (
	"net/http"
	"time"
)

//Health is a type
type Health struct {
	handler     http.Handler
	allowedHost string
}

//HealthHandler is a Handler
func HealthHandler(handler http.Handler) *Health {
	return &Health{handler: handler}
}

//HealthResponse is a type
type HealthResponse struct {
	Time     time.Time
	Message  string
	Channels map[string]bool
}

func (s *Health) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var gol HealthResponse
	Proxier(w, r, gol)
}
