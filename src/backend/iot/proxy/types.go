package proxy

import (
	"encoding/json"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"net/http"
	"github.com/gorilla/context"
	"log"
)

type IotAction struct {
	Username string        `json:"username"`
	Hostname string        `json:"hostname"`
	Channel string        `json:"channel"`
	State string        `json:"state"`

}




//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeActionJSON() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")
			var device IotAction
			log.Println(r.Body)

			err := json.NewDecoder(r.Body).Decode(&device)
			if err != nil {
				log.Println("ERROO DECODER")
				http.Error(w, "Internal server error.", http.StatusBadRequest)
			} else {
				context.Set(r, "action", device)
				h.ServeHTTP(w, r)
			}
		})
	}
}
