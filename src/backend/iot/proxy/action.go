package proxy

import (
	"net/http"
	"time"
)

//Action is a type
type Action struct {
	handler     http.Handler
	allowedHost string
}

//ActionHandler is a Handler
func ActionHandler(handler http.Handler) *Action {
	return &Action{handler: handler}
}

//ActionResponse is a type
type ActionResponse struct {
	Time    time.Time
	Message string
}

func (s *Action) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var responseStruct ActionResponse
	Proxier(w, r, responseStruct)
}
