package proxy

import (
	"net/http"
	"time"

	"github.com/gorilla/context"
	"encoding/json"

	"bitbucket.org/cescoferraro/api/src/backend/iot/ws"
	"log"
	"bytes"
	"crypto/tls"
)

//Index is a type
type Index struct {
	handler     http.Handler
	allowedHost string
}

//IndexHandler is a Handler
func IndexHandler(handler http.Handler) *Index {
	return &Index{handler: handler}
}

//IndexResponse is a type
type IndexResponse struct {
	Time     time.Time
	Message  string
	Channels map[string]bool
}

func (s *Index) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var gol IndexResponse
	indexProxier(w, r, gol)
}





//Proxier is a function that proxyes the request from the main server to the device, is related to PROXY MODE
func indexProxier(w http.ResponseWriter, r *http.Request, responseStruct interface{}) {
	w.Header().Set("Content-Type", "application/json")
	action := context.Get(r, "action").(IotAction)
	url := "https://" + action.Hostname + ".ngrok.io/lights/" + action.Channel + "/" + action.State
	var cesco IndexResponse
	body, _ := json.Marshal(cesco)

	req, err := http.NewRequest("GET", url, bytes.NewBuffer(body))
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer resp.Body.Close()




	log.Println(action)
	err = json.NewDecoder(resp.Body).Decode(&responseStruct)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ws.Publish("dashboard", "L1@@"+action.Hostname + "@@" + action.Channel + "@@" + action.State +"@@" +action.Username)
	text, err := json.Marshal(responseStruct)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusBadRequest)
		return
	}
	w.WriteHeader(200)
	w.Write(text)

}
