package proxy

import (
	"net/http"
	"time"
)

//Status is a type
type Status struct {
	handler     http.Handler
	allowedHost string
}

//StatusHandler is a Handler
func StatusHandler(handler http.Handler) *Status {
	return &Status{handler: handler}
}

//StatusResponse is a type
type StatusResponse struct {
	Time     time.Time
	Message  string
	Channels map[string]bool
}

func (s *Status) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var gol StatusResponse
	Proxier(w, r, gol)
}
