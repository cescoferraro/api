package proxy

import (
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) *mux.Router {

	proxyRouter := router.PathPrefix("/iot/proxy").Subrouter()
	proxyRouter.Handle("/{device}/lights/status", util.Adapt(StatusHandler(router),
		util.EnableCORS(),
		util.ExpectGET(),
	))

	proxyRouter.Handle("/{device}/lights/health", util.Adapt(HealthHandler(router),
		util.EnableCORS(),
		util.ExpectGET(),
	))



	proxyRouter.Handle("/", util.Adapt(IndexHandler(router),
		EncodeActionJSON(),
		util.EnableCORS(),
		util.ExpectPOST(),
	))
	return proxyRouter

}
