package proxy

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"bytes"
	"crypto/tls"

	"bitbucket.org/cescoferraro/api/src/backend/iot/device"
	"github.com/gorilla/mux"
)

//Proxier is a function that proxyes the request from the main server to the device, is related to PROXY MODE
func Proxier(w http.ResponseWriter, r *http.Request, responseStruct interface{}) {

	w.Header().Set("Content-Type", "application/javascript")
	vars := mux.Vars(r)
	devicethis := vars["device"]

	log.Println(devicethis)
	str := strings.Replace(r.URL.String(), "/iot/proxy/"+devicethis, "", -1)
	log.Println(str)

	ERRR, _ := json.Marshal(&device.Device{})

	req, err := http.NewRequest("GET", "https://"+devicethis+".ngrok.io"+str, bytes.NewBuffer(ERRR))
	tr := &http.Transport{
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&responseStruct)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "Internal server error.", http.StatusBadRequest)
	}

	text, err := json.Marshal(responseStruct)
	if err != nil {
		log.Println("error:", err)
	}
	w.WriteHeader(200)
	w.Write(text)

}
