package ws

import (
	"github.com/gorilla/websocket"
	"github.com/spf13/viper"
	"gopkg.in/redis.v3"
	"log"
	"net/http"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)



var upgrader = websocket.Upgrader{
	CheckOrigin:     func(r *http.Request) bool {
		return true
	},
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}




//Login is a type
type WS struct {
	handler     http.Handler
	allowedHost string
}

//LoginHandler is a Login Handler
func WSHandler(handler http.Handler) *WS {
	return &WS{handler: handler}
}

// connection is an middleman between the websocket connection and the hub.
type connection struct {
	// The websocket connection.
	ws   *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

func (s *WS) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}

	c := &connection{send: make(chan []byte, 256), ws: ws}

	log.Println("registered")
	DashboardHub.register <- c
	go c.WritePump("dashboard")
	c.ReadPump("dashboard")
}




// readPump pumps messages from the websocket connection to the hub.
func (c *connection) ReadPump(channel string) {
	log.Println("readPump")
	defer func() {
		DashboardHub.unregister <- c
		c.ws.Close()
	}()
	c.ws.SetReadLimit(maxMessageSize)
	c.ws.SetReadDeadline(time.Now().Add(pongWait))
	c.ws.SetPongHandler(func(string) error {
		c.ws.SetReadDeadline(time.Now().Add(pongWait)); return nil
	})
	for {
		_, message, err := c.ws.ReadMessage()
		log.Println("just received : ", string(message))
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
			}
			break
		}
		log.Println("before publishing TO REDIS " + channel)

		Publish(channel , string(message))
	}
}

// write writes a message with the given message type and payload.
func (c *connection) write(mt int, payload []byte) error {
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}


// writePump pumps messages from the hub to the websocket connection.
func (c *connection) WritePump(channelname string) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.ws.Close()
	}()
	for {
		select {

		case message, ok := <-c.send:
			if !ok {

				c.write(websocket.CloseMessage, []byte{})
				return
			}
			err := c.write(websocket.TextMessage, message)
			if err != nil {
				return
			}else {
				log.Println("just sendeed ",string(message), " to ", channelname)

			}

		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func Publish(channelname, msg string) {

	log.Println("publishing " + msg + " to REDIS")
	client := redis.NewClient(&redis.Options{
		Addr:     viper.GetString("redis_host") + ":" + viper.GetString("redis_port"),
		Password: "descriptor8", // no password set
		DB:       0, // use default DB
	})
	err := client.Publish(viper.GetString("project_name")+"/"+ channelname, msg).Err()
	if err != nil {
		panic(err)
	}
}
