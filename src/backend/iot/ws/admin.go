package ws

import (
	"log"
	"net/http"

)






//Login is a type
type AdminWS struct {
	handler     http.Handler
	allowedHost string
}

//LoginHandler is a Login Handler
func AdminWSHandler(handler http.Handler) *AdminWS {
	return &AdminWS{handler: handler}
}


func (s *AdminWS) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}

	c := &connection{send: make(chan []byte, 256), ws: ws}

	log.Println("registered")
	AdminHub.register <- c
	go c.WritePump("admin")
	c.ReadPump("admin")
}

