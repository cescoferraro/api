package ws

import (
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func WsRoutes(router *mux.Router) {
	go DashboardHub.Run()
	wsRouter := router.PathPrefix("/iot").Subrouter()
	wsRouter.Handle("/lights/ws", util.Adapt(WSHandler(router),
		util.EnableCORS(),
	))
	go AdminHub.Run()
	wsRouter.Handle("/admin/ws", util.Adapt(AdminWSHandler(router),
		util.EnableCORS(),
	))
	return
}
