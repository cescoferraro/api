package ws

import (
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
	"github.com/spf13/viper"
	"gopkg.in/redis.v3"
)

//InitRedis is a function that subscribe to a Redis pub/sub channel
func InitRedisChannel(channelname string, done chan bool) {
	util.LogIfVerbose(color.FgRed, "REDIS", "Attempting to create channel"+channelname)
	url := viper.GetString("redis_host") + ":" + viper.GetString("redis_port")
	client := redis.NewClient(&redis.Options{
		Addr:     url,
		Password: viper.GetString("redis_password"), // no password set
		DB:       0,                                 // use default DB
	})
	pubsub, err := client.Subscribe(viper.GetString("project_name") + "/" + channelname)
	for err != nil {
		time.Sleep(10 * time.Second)
		pubsub, err = client.Subscribe(viper.GetString("project_name") + "/" + channelname)
	}
	defer pubsub.Close()

	util.LogIfVerbose(color.FgRed, "REDIS", "Connected")
	done <- true

	for {
		msg, err := pubsub.ReceiveMessage()
		if err != nil {
			panic(err)
		}

		util.LogIfVerbose(color.FgRed, "REDIS", " Channel "+channelname+" received "+msg.Payload)
		switch channelname {
		case "dashboard":
			DashboardHub.Broadcast <- []byte(msg.Payload)
		case "admin":
			AdminHub.Broadcast <- []byte(msg.Payload)
		}

	}

}
