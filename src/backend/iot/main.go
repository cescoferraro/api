package iot

import (
	"bitbucket.org/cescoferraro/api/src/backend/iot/device"
	"bitbucket.org/cescoferraro/api/src/backend/iot/house"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/iot/proxy"
	"bitbucket.org/cescoferraro/api/src/backend/iot/user"
	"bitbucket.org/cescoferraro/api/src/backend/iot/ws"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
	"github.com/gorilla/mux"

	"bitbucket.org/cescoferraro/api/src/backend/iot/scheduler"
)

func RunDependencies() {

	util.LogIfVerbose(color.FgRed, " IOT ", "Starting Dependencies")
	done1 := make(chan bool, 1)
	done2 := make(chan bool, 1)
	done3 := make(chan bool, 1)

	go mongo.InitMongo(done1)
	<-done1
	go ws.InitRedisChannel("dashboard", done2)

	<-done2

	go ws.InitRedisChannel("admin", done3)
	<-done3

	util.LogIfVerbose(color.FgRed, " IOT ", "Finished Dependencies")
}

func InitRouter(router *mux.Router) *mux.Router {
	device.DeviceRoutes(router)
	house.HouseRoutes(router)
	proxy.Routes(router)
	user.UserRoutes(router)
	ws.WsRoutes(router)
	scheduler.Routes(router)
	return router
}
