package scheduler

import (
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) *mux.Router {

	houseRouter := router.PathPrefix("/iot/scheduler").Subrouter()
	houseRouter.Handle("/create", util.Adapt(CreateHandler(router),
		EncodeIotJobJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))


	houseRouter.Handle("/tasks/", util.Adapt(TasksHandler(router),
		EncodeTaskRequestJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))


	houseRouter.Handle("/read", util.Adapt(ReadHandler(router),
		EncodeIotJobJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	houseRouter.Handle("/delete", util.Adapt(DeleteHandler(router),
		EncodeIotJobJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))


	houseRouter.Handle("/delete/all", util.Adapt(WipeHandler(router),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	return houseRouter
}
