package scheduler

import (
	"encoding/json"

	"net/http"

	"log"
	"github.com/gorilla/context"
)



//Register is a type
type Delete struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func DeleteHandler(handler http.Handler) *Delete {
	return &Delete{handler: handler}
}

func (s *Delete) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	job := context.Get(r, "scheduler").(IotJob)
	log.Println(job)
	action := IotJob{Id:job.Id}
	_, err := action.delete()
	if err != nil {
		http.Error(w, "Bad server", 404)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	log.Println("deleted!!!!!")
	json, _ := json.Marshal(action)
	w.WriteHeader(200)
	w.Write(json)
	return

}




