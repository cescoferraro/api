package scheduler

import (
	"encoding/json"

	"net/http"

	"log"
	"github.com/gorilla/context"
	"github.com/ajvb/kala/job"
)



//Register is a type
type Tasks struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func TasksHandler(handler http.Handler) *Tasks {
	return &Tasks{handler: handler}
}

func (s *Tasks) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	taskRequest := context.Get(r, "tasks").(TaskRequest)
	log.Println(taskRequest.Device)

	taskss, _ := IotJob{Device:taskRequest.Device}.getTasks()

	var rr *job.Job
	var i int

	for i, rr = range taskss {
		log.Println(i, rr.Owner)
	}

	var response []byte
	if len(taskss) != 0 {
		response, _ = json.Marshal(taskss)
	} else {
		response, _ = json.Marshal(make([]*job.Job, 0))
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(response)
	return
}




