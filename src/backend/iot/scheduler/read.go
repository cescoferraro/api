package scheduler

import (
	"encoding/json"

	"net/http"

	"log"
	"github.com/gorilla/context"
)



//Register is a type
type Read struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func ReadHandler(handler http.Handler) *Read {
	return &Read{handler: handler}
}

func (s *Read) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	job := context.Get(r, "scheduler").(IotJob)
	log.Println(job)

	id,err :=job.getTasks()
	if err !=nil{
		http.Error(w,"Read Error", 404)
	}
	json, _ := json.Marshal(id)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(json)
	return
}




