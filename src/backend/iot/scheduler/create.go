package scheduler

import (
	"encoding/json"

	"net/http"

	"log"
	"github.com/gorilla/context"
)



//Register is a type
type Create struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func CreateHandler(handler http.Handler) *Create {
	return &Create{handler: handler}
}

func (s *Create) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	job := context.Get(r, "scheduler").(IotJob)
	log.Println(job)

	id,err :=job.ping()
	if err !=nil{
		http.Error(w,err.Error(), 404)
		return
	}
	log.Println(id)
	job.Id = id
	json, _ := json.Marshal(job)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(json)
	return
}




