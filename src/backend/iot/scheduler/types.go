package scheduler

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/ajvb/kala/client"
	"github.com/ajvb/kala/job"
	"github.com/gorilla/context"
	"github.com/spf13/viper"
)

type TaskRequest struct {
	Device string `json:"device"`
}

type IotJob struct {
	Id       string `json:"id"`
	Username string `json:"username"`
	Device   string `json:"device"`
	Channel  int    `json:"channel"`
	State    bool   `json:"state"`
	Delay    int    `json:"delay"`
	Repeats  int    `json:"repeat"`
	Interval int    `json:"interval"`
}

func (action IotJob) cmd() string {
	var production string
	if os.Getenv("KUBERNETES") == "true" {
		production = "--production"
	} else {
		production = ""
	}
	return "go run tasks/action/main.go --device=" + action.Device + " --channel=" + strconv.Itoa(action.Channel) + " --state=" + strconv.FormatBool(action.State) + " --username=" + action.Username + " " + production
}

func (action IotJob) getSchedule() string {

	realdelay := time.Duration(action.Delay) * time.Second
	thisTime := time.Now().Add(realdelay).Add(200 * time.Millisecond).Format(time.RFC3339Nano)

	var actualreps string
	if action.Repeats == 10000 {
		actualreps = ""
	} else {
		actualreps = strconv.Itoa(action.Repeats)
	}

	return "R" + actualreps + "/" + thisTime + "/PT" + strconv.Itoa(action.Interval) + "S"
}

func (action IotJob) url() string {
	return "http://" + viper.GetString("scheduler_host") + ":" + viper.GetString("scheduler_port")
}

func (action IotJob) ping() (string, error) {
	c := client.New(action.url())
	body := &job.Job{
		Schedule: action.getSchedule(),
		Name:     action.cmd(),
		Owner:    action.Device,
		//Command:  "",
		Command: action.cmd(),
	}
	return c.CreateJob(body)

}

func (action IotJob) getTasks() ([]*job.Job, error) {
	c := client.New(action.url())
	all, _ := c.GetAllJobs()
	var deviceJobs []*job.Job
	for _, v := range all {
		if action.Device == "all" {
			if v.Owner == action.Device && v.IsDone == false {
				deviceJobs = append(deviceJobs, v)
			}
		} else {
			if v.Owner == action.Device && v.IsDone == false {
				deviceJobs = append(deviceJobs, v)
			}
		}

	}
	if len(deviceJobs) != 0 {
		return deviceJobs, nil
	} else {
		return make([]*job.Job, 0), nil
	}

}

func (action IotJob) delete() (bool, error) {
	c := client.New(action.url())
	return c.DeleteJob(action.Id)

}

func (action IotJob) deleteAll() error {
	log.Println(action, "heyyyyy")
	c := client.New(action.url())
	all, err := c.GetAllJobs()
	if err != nil {
		return errors.New("Job not found")
	}
	log.Println(all)
	for _, v := range all {
		_, err = c.DeleteJob(v.Id)
		if err != nil {
			return errors.New("Job not found")
		}
	}
	return nil

}

//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeIotJobJSON() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")

			//hah, err := ioutil.ReadAll(r.Body);
			//
			//if err != nil {
			//	log.Println(err.Error())
			//}
			//
			//log.Println(string(hah))

			var iotJob IotJob
			log.Println("sdfnfjksdndfkjsdndfjknsdfkjndsnfjkdsdf")
			err := json.NewDecoder(r.Body).Decode(&iotJob)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			} else {
				log.Println("sdfnfjksdndfkjsdndfjknsdfkjndsnfjkdsdf")
				context.Set(r, "scheduler", iotJob)
				h.ServeHTTP(w, r)
			}
		})
	}
}

//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeTaskRequestJSON() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")
			var TaskRequest TaskRequest
			err := json.NewDecoder(r.Body).Decode(&TaskRequest)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			} else {
				context.Set(r, "tasks", TaskRequest)
				h.ServeHTTP(w, r)
			}
		})
	}
}
