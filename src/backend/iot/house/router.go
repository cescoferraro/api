package house

import (
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func HouseRoutes(router *mux.Router) *mux.Router {

	houseRouter := router.PathPrefix("/iot/house").Subrouter()
	houseRouter.Handle("/create", util.Adapt(CreateHandler(router),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	houseRouter.Handle("/read", util.Adapt(ReadHandler(router),
		mongo.IotMongoDatabaseConnection(),
		EncodeHouseJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))
	houseRouter.Handle("/delete", util.Adapt(DeleteHandler(router),
		mongo.IotMongoDatabaseConnection(),
		EncodeHouseJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))
	houseRouter.Handle("/update", util.Adapt(UpdateHandler(router),
		mongo.IotMongoDatabaseConnection(),
		EncodeHouseJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	return houseRouter
}
