package house

import (
	"encoding/json"
	"net/http"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
)



//Register is a type
type Read struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func ReadHandler(handler http.Handler) *Read {
	return &Read{handler: handler}
}

func (s *Read) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	house := context.Get(r, "house").(House)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	var result House
	if house.ID.Valid() {
		err := db.HouseCollection.FindId(house.ID).One(&result)
		if err != nil {
			http.Error(w, "Bad Server", http.StatusNotFound)
			return
		}

		hey, _ := json.Marshal(result)
		w.WriteHeader(200)
		w.Write(hey)
	} else {
		http.Error(w, "Bad Server", http.StatusNotFound)
	}

}







