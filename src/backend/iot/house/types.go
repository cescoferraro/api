package house

import (
	"gopkg.in/mgo.v2/bson"
	"time"
	"encoding/json"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"net/http"
	"github.com/gorilla/context"
)

type House struct {
	ID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name    string        `json:"name"`
	Devices []bson.ObjectId        `json:"devices"`
	Users	[]bson.ObjectId	`json:"users"`
}


//RegisterResponse is a type
type HouseResponse struct {
	Time     time.Time
	Timestamp int64
	Response string
}


//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeHouseJSON() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")
			var house House
			err := json.NewDecoder(r.Body).Decode(&house)
			if err != nil {
				http.Error(w, "Internal server error.", http.StatusBadRequest)
			} else {
				context.Set(r, "house", house)
				h.ServeHTTP(w, r)
			}
		})
	}
}
