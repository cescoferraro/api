package house

import (
	"encoding/json"

	"net/http"

	"log"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"

	"time"
)



//Register is a type
type Delete struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func DeleteHandler(handler http.Handler) *Delete {
	return &Delete{handler: handler}
}

func (s *Delete) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mongo.IotDatabase)
	house := context.Get(r, "house").(House)
	log.Println(house)
	err := db.HouseCollection.RemoveId(house.ID)
	if err != nil {
		log.Println(err.Error())
		ww := HouseResponse{
			Time:     time.Now(),
			Timestamp:     time.Now().Unix(),
			Response: err.Error(),
		}
		hey, _ := json.Marshal(ww)
		http.Error(w, string(hey), http.StatusInternalServerError)
		return
	}

	ee := HouseResponse{
		Time:     time.Now(),
		Timestamp:     time.Now().Unix(),
		Response: "House Deleted",
	}

	hey, _ := json.Marshal(ee)
	w.WriteHeader(200)
	w.Write(hey)
	return
}





