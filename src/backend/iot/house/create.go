package house

import (
	"encoding/json"

	"net/http"
	"time"

	"log"
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
)



//Register is a type
type Create struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func CreateHandler(handler http.Handler) *Create {
	return &Create{handler: handler}
}

func (s *Create) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mongo.IotDatabase)

	var house House
	_ = json.NewDecoder(r.Body).Decode(&house)

	if house.ID.Valid() != true {
		house.ID = bson.NewObjectId()
	}

	house.Devices = make([]bson.ObjectId, 0)

	err := db.HouseCollection.Insert(house)
	if err != nil {
		log.Println(err.Error())
		ww := HouseResponse{
			Time:     time.Now(),
			Response: err.Error(),
		}
		hey, _ := json.Marshal(ww)
		http.Error(w, string(hey), http.StatusInternalServerError)
		return
	}

	hey, _ := json.Marshal(house)
	w.WriteHeader(200)
	w.Write(hey)
	return
}





