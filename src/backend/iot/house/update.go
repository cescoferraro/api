package house

import (
	"encoding/json"

	"net/http"

	"log"

	"time"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
)

//Register is a type
type Update struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func UpdateHandler(handler http.Handler) *Update {
	return &Update{handler: handler}
}

func (s *Update) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	house := context.Get(r, "house").(House)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	log.Println(house.Name)
	var result House

	err := db.HouseCollection.FindId(house.ID).One(&result)
	if err != nil {
		log.Println("FIRRRRRRRRR")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Println(result)
	err = db.HouseCollection.Update(result, house)
	if err != nil {
		log.Println("FIRRRRRRRRSDFSDFSDFR")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ee := HouseResponse{
		Time:      time.Now(),
		Timestamp: time.Now().Unix(),
		Response:  "House Updated",
	}

	hey, _ := json.Marshal(ee)
	w.WriteHeader(200)
	w.Write(hey)
	return
}
