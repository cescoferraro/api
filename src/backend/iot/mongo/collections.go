package mongo

import (
	"time"

	"github.com/fatih/color"

	"bitbucket.org/cescoferraro/api/src/backend/util"

	"gopkg.in/mgo.v2"
)

func (db *IotDatabase) SetIotCollections(done chan bool) {
	util.LogIfVerbose(color.FgRed, "MONGO", "Starting Collection")
	db.setDeviceLogCollection()
	db.setUserCollection("users", "username", "email")
	db.setDeviceCollection("devices", "hostname")
	db.setHouseCollection("house", "name")
	util.LogIfVerbose(color.FgRed, "MONGO", "All collection setted")
	done <- true
}

func (db *IotDatabase) setDeviceLogCollection() {
	db.DeviceLogCollection = &IotCollection{db.Session.DB(db.Name).C("deviceLog")}
	db.DeviceLogCollection.Create(&mgo.CollectionInfo{
		Capped:   true,
		MaxBytes: 150000000,
		MaxDocs:  200,
	})
	util.LogIfVerbose(color.FgRed, "MONGO", "DeviceLog collection setted")
}

func (db *IotDatabase) setHouseCollection(CollectionName string, unique ...string) {
	var err error
	c := &IotCollection{db.Session.DB(db.Name).C(CollectionName)}
	for num := range unique {
		db.HouseCollection, err = c.EnsureUnique(unique[num])
	}
	for err != nil {
		time.Sleep(2 * time.Second)
		util.LogIfVerbose(color.FgRed, "MONGO", "Retrying House Collection")
		for num := range unique {
			db.HouseCollection, err = c.EnsureUnique(unique[num])
		}
	}
	util.LogIfVerbose(color.FgRed, "MONGO", "House collection setted")
}

func (db *IotDatabase) setUserCollection(CollectionName string, unique ...string) {
	var err error
	c := &IotCollection{db.Session.DB(db.Name).C(CollectionName)}
	for num := range unique {
		db.UserCollection, err = c.EnsureUnique(unique[num])
	}
	for err != nil {
		time.Sleep(2 * time.Second)
		util.LogIfVerbose(color.FgRed, "MONGO", "Retrying User Collection")
		for num := range unique {
			db.UserCollection, err = c.EnsureUnique(unique[num])
		}
	}
	util.LogIfVerbose(color.FgRed, "MONGO", "User collection setted")
}

func (db *IotDatabase) setDeviceCollection(CollectionName string, unique ...string) {
	var err error
	c := &IotCollection{db.Session.DB(db.Name).C(CollectionName)}
	for num := range unique {
		db.DeviceCollection, err = c.EnsureUnique(unique[num])
	}

	for err != nil {
		time.Sleep(2 * time.Second)
		util.LogIfVerbose(color.FgRed, "MONGO", "Retrying Device Collection")
		for num := range unique {
			db.DeviceCollection, err = c.EnsureUnique(unique[num])
		}
	}
	util.LogIfVerbose(color.FgRed, "MONGO", "Device collection setted")
}

//EnsureUnique is a function that ensure that wont be any duplicates on a given collection
func (c *IotCollection) EnsureUnique(unique ...string) (*IotCollection, error) {
	index := mgo.Index{
		Key:      unique,
		Unique:   true,
		DropDups: true,
	}
	err := c.EnsureIndex(index)
	return c, err
}
