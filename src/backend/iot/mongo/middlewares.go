package mongo

import (
	"net/http"

	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
	"github.com/gorilla/context"
)

//GetMongoConnection is a middleware
func IotMongoDatabaseConnection() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			db := new(IotDatabase).Cloner()
			if err := db.Session.Ping(); err != nil {
				util.LogIfVerbose(color.FgRed, "MONGO", "Connection went OK with MONGO, but ping failed!")
				//SendAPIResponse(w, 200, "Empty fields")
				return
			}
			defer db.Session.Close()
			context.Set(r, "db", db)
			h.ServeHTTP(w, r)

		})
	}
}
