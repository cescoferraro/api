package mongo

import "gopkg.in/mgo.v2"

//MONGO local MongoObject
var IOT_DB IotDatabase

//MongoObject mgo.Session
type IotDatabase struct {
	Name                string
	Session             *mgo.Session
	UserCollection      *IotCollection
	DeviceCollection    *IotCollection
	DeviceLogCollection *IotCollection
	HouseCollection     *IotCollection
}

type IotCollection struct {
	*mgo.Collection
}

//Cloner clones the local MONGO object
func (_ IotDatabase) Cloner() *IotDatabase {
	return &IotDatabase{
		"iot",
		IOT_DB.Session.Copy(),
		IOT_DB.UserCollection,
		IOT_DB.DeviceCollection,
		IOT_DB.DeviceLogCollection,
		IOT_DB.HouseCollection,
	}
}
