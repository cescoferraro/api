package mongo

import (
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
)

func (db *IotDatabase) SetSession() *IotDatabase {
	util.LogIfVerbose(color.FgRed, "MONGO", "Attempting to set Session")

	var err error
	db.Session, err = Connect()
	for err != nil {
		time.Sleep(2 * time.Second)
		util.LogIfVerbose(color.FgRed, "MONGO", "Trying connection with mongo again")

		db.Session, err = Connect()
	}
	util.LogIfVerbose(color.FgRed, "MONGO", "Sessions connected")
	return db
}

//Connect is a function that connects to MOngoDB
func Connect() (*mgo.Session, error) {

	var err error
	info, err := mgo.ParseURL(viper.GetString("MONGO_USER") +
		":" + viper.GetString("MONGO_PASS") +
		"@" + viper.GetString("MONGO_HOST") +
		":" + viper.GetString("MONGO_PORT"))
	session, err := mgo.DialWithInfo(info)
	if err != nil {
		util.LogIfError(err)
		util.LogIfVerbose(color.FgRed, "MONGO", "Connection failed!")
		return session, err
	}
	if err = session.Ping(); err != nil {
		util.LogIfError(err)
		util.LogIfVerbose(color.FgRed, "MONGO", "Connection went OK, but ping failed!")
		return session, err
	}
	session.SetMode(mgo.Monotonic, true)
	return session, err
}
