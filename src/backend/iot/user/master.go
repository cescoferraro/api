package user

import (
	"net/http"
	"encoding/json"
	"github.com/gorilla/context"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
)

//Master is a type
type Master struct {
	handler     http.Handler
	allowedHost string
}

//MasterHandler is a Master Handler
func MasterHandler(handler http.Handler) *Master {
	return &Master{handler: handler}
}

func (s *Master) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	user := context.Get(r, "user").(User)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	var result User
	err := db.UserCollection.FindId(user.ID).One(&result)
	if err != nil {
		http.Error(w, "bad", http.StatusNotFound)
	}

	if result.Role == "master" {
		hey, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write(hey)

	} else {

		http.Error(w, "bad", http.StatusNotFound)
	}

}
