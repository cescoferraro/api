package user

import (
	"encoding/json"
	"net/http"

	"log"
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"

	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

//Login is a type
type Login struct {
	handler     http.Handler
	allowedHost string
}

//LoginHandler is a Login Handler
func LoginHandler(handler http.Handler) *Login {
	return &Login{handler: handler}
}

func (s *Login) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/javascript")
	user := context.Get(r, "user").(User)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	result := User{}
	err := db.UserCollection.Find(bson.M{"email": user.Email}).One(&result)
	if err != nil {
		SendLoginResponse(w, http.StatusUnauthorized, "User not found!", "")
		return
	}

	bs := bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))
	// if result.Confirmed == true {
	if bs != nil {
		SendLoginResponse(w, http.StatusUnauthorized, "User exist BUT paswword DONT match", "")
	}

	tokenString, err := CreateToken(result)
	if err != nil {
		SendLoginResponse(w, http.StatusUnauthorized, "Error creating jwt!", "")
	}
	SendLoginResponse(w, 200, "All good", tokenString)

	// } else {
	//
	// 	SendApiResponse(w, http.StatusUnauthorized,  "User not confirmed")
	// }

	return

}

//SendLoginResponse send an APIResponse encoded in json
func SendLoginResponse(w http.ResponseWriter, status int, reason, token string) {
	//ResponseAPI is a type
	type LoginResponse struct {
		Time     time.Time
		Response string
		Jwt      string
	}

	this := LoginResponse{
		Time:     time.Now(),
		Response: reason,
		Jwt:      token,
	}
	hey, _ := json.Marshal(this)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(hey)

}

//CreateToken creates a token

func CreateToken(result User) (string, error) {
	mySigningKey := []byte(viper.GetString("privateKey"))
	type MyCustomClaims struct {
		Devices []string      `json:"devices"`
		Role    string        `json:"role"`
		Email   string        `json:"email"`
		Id      bson.ObjectId `json:"id"`
		jwt.StandardClaims
	}
	// Create the Claims
	claims := MyCustomClaims{
		result.Devices,
		result.Role,
		result.Email,
		result.ID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 10).Unix(),
			Issuer:    "test",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		log.Println(err.Error())
	}
	return tokenString, err
}
