package user

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/iot/ws"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
)

//Status is a type
type UPDATE struct {
	handler     http.Handler
	allowedHost string
}

//StatusHandler is a Handler
func UPDATEHandler(handler http.Handler) *UPDATE {
	return &UPDATE{handler: handler}
}

func (s *UPDATE) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	user := context.Get(r, "user_update").(User)
	db := context.Get(r, "db").(*mongo.IotDatabase)

	var result User
	err := db.UserCollection.FindId(user.ID).One(&result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = db.UserCollection.Update(result, user)
	if err != nil {
	}
	var buffer bytes.Buffer
	for _, mm := range user.Devices {
		buffer.WriteString("@@")
		buffer.WriteString(mm)

	}

	ws.Publish("dashboard","L2@@"+ user.ID.Hex()+buffer.String())

	type UPDATEResponse struct {
		Time    time.Time
		Success bool
	}

	this := UPDATEResponse{
		Time: time.Now(),
	}
	hey, _ := json.Marshal(this)
	w.WriteHeader(200)
	w.Write(hey)

}
