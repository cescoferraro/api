package user

import (
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func UserRoutes(router *mux.Router) {
	cesco := router.PathPrefix("/iot/user").Subrouter()
	cesco.Handle("/all", util.Adapt(AllHandler(router),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectGET(),
		util.EnableCORS(),
	))

	cesco.Handle("/register", util.Adapt(CreateHandler(router),
		EmptyRegisterUserForm("userRegister"),
		EncodeUserJSON("userRegister"),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectBody(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	cesco.Handle("/login", util.Adapt(LoginHandler(router),
		EncodeUserJSON("user"),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectBody(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	cesco.Handle("/authenticate", util.Adapt(AuthenticateHandler(router),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	cesco.Handle("/update", util.Adapt(UPDATEHandler(router),
		EncodeUserJSON("user_update"),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectBody(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))



	cesco.Handle("/read", util.Adapt(CheckUserHandler(router),
		//Bouncer(),
		EncodeUserJSON("user"),
		mongo.IotMongoDatabaseConnection(),
		ValidJWT(),
		util.ExpectBody(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	cesco.Handle("/master", util.Adapt(CheckUserHandler(router),
		//Bouncer(),
		EncodeUserJSON("user"),
		mongo.IotMongoDatabaseConnection(),
		//ValidJWT(),
		util.ExpectBody(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	return

}
