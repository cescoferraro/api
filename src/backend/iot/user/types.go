package user

import (
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"encoding/json"
	"net/http"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"log"
	"github.com/gorilla/context"
	"github.com/dgrijalva/jwt-go"
	"fmt"
	"github.com/spf13/viper"
	"github.com/dgrijalva/jwt-go/request"
)

//Users type for the above middleware
type User struct {
	ID           bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Username     string        `json:"username"`
	Email        string        `json:"email"`
	Password     string        `json:"password"`
	Role         string        `json:"role"`
	Devices      []string      `json:"devices"`
	Confirmed    bool          `json:"confirmed"`
	Notification bool          `json:"notification"`
}



//CreateUser Insert the user on a database
func CreateUser(c *mongo.IotCollection, user User) error {
	user.ID = bson.NewObjectId()
	if user.Email == "francescoaferraro@gmail.com" {
		user.Role = "master"
	} else {
		user.Role = "user"
	}
	err := c.Insert(user)
	return err

}


//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeUserJSON(thiscontext string) util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")
			var user User
			err := json.NewDecoder(r.Body).Decode(&user)
			if err != nil {
				http.Error(w, "Internal server error.", http.StatusBadRequest)
			} else {
				log.Println(user.ID)
				context.Set(r, thiscontext, user)
				h.ServeHTTP(w, r)
			}
		})
	}
}



//EmptyRegisterUserForm is a middleware
func EmptyRegisterUserForm(thiscontext string) util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := context.Get(r, thiscontext).(User)
			if user.Email != "" && user.Password != "" && user.Username != "" {
				h.ServeHTTP(w, r)
			} else {
				util.SendAPIResponse(w, http.StatusLengthRequired, "Empty fields")
			}

		})
	}
}



//EmptyRegisterUserForm is a middleware
func ValidJWT() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			token, err := request.ParseFromRequest(r, request.HeaderExtractor{}, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
					SendAuthenticateResponse(w, http.StatusUnauthorized, "GO HOME JWT HACKER")
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}
				return []byte(viper.GetString("publicKey")), nil
			})

			log.Println(token)


			if err == nil && token.Valid {

				context.Set(r, "token", token)
				h.ServeHTTP(w, r)
			}
				h.ServeHTTP(w, r)

//else {
//				util.SendAPIResponse(w, http.StatusUnauthorized, "Invalid Token")
//			}
		})
	}
}


//EmptyRegisterUserForm is a middleware
//func Bouncer(roles ...string) util.Adapter {
//	return func(h http.Handler) http.Handler {
//		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//			tokens := context.Get(r, "token").(*jwt.Token)
//			userRole, _ := tokens.Claims["role"].(string)
//			for _, role := range roles {
//				if role == userRole {
//					h.ServeHTTP(w, r)
//					return
//				}
//			}
//			id, _ := tokens.Claims["id"].(string)
//			user := context.Get(r, "user").(User)
//			if bson.ObjectIdHex(id) == user.ID {
//				log.Println("same user")
//				h.ServeHTTP(w, r)
//				return
//			}
//			util.SendAPIResponse(w, http.StatusUnauthorized, "Invalid Token")
//
//		})
//	}
//}

