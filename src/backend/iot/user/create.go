package user

import (
	"bytes"
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/iot/ws"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/context"
	"github.com/mailgun/mailgun-go"
	"golang.org/x/crypto/bcrypt"
)

//Register is a type
type Create struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func CreateHandler(handler http.Handler) *Create {
	return &Create{handler: handler}
}

func (s *Create) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	user := context.Get(r, "userRegister").(User)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	bs, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)
	user.Password = string(bs)
	if user.Email == "francescoaferraro@gmail.com" {
		user.Confirmed = true
	} else {
		user.Confirmed = false
	}
	err := CreateUser(db.UserCollection, user)
	if err != nil {
		util.LogIfError(err)
		SendRegisterResponse(w, http.StatusConflict, err.Error())
		return
	}
	ws.Publish("admin", "hello")
	SendRegisterResponse(w, http.StatusOK, "User Added")

}

//SendRegisterResponse send an APIResponse encoded in json
func SendRegisterResponse(w http.ResponseWriter, status int, reason string) {
	//RegisterResponse is a type
	type CreateResponse struct {
		Time     time.Time
		Response string
	}
	this := CreateResponse{
		Time:     time.Now(),
		Response: reason,
	}

	hey, _ := json.Marshal(this)
	w.WriteHeader(status)
	w.Write(hey)

}

//SendSimpleMessage send a email through mailgun
func SendSimpleMessage(recipient, file string, variables map[string]string) (string, error) {
	mg := mailgun.NewMailgun("cescoferraro.xyz", "key-3addcf54633379b545d844d7e9cdad65", "pubkey-d5d076d45fcb61b0be3ac248eee4e785")
	m := mg.NewMessage(
		"iot <mailgun@cescoferraro.xyz>",
		"Hello",
		"Testing some Mailgun awesomeness!",
		recipient,
	)
	var err error
	var tpl *template.Template
	tpl, err = tpl.ParseFiles("./ts/html/register.html")

	buf := new(bytes.Buffer)
	err = tpl.ExecuteTemplate(buf, "register.html", variables)
	if err != nil {
		log.Fatalln(err)
	}
	m.SetHtml(buf.String())
	_, id, err := mg.Send(m)
	log.Println("email sent")
	return id, err
}
