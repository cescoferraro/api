package user

import (
	"net/http"
	"encoding/json"
	"github.com/gorilla/context"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
)

//CheckUser is a type
type CheckUser struct {
	handler     http.Handler
	allowedHost string
}

//CheckUserHandler is a CheckUser Handler
func CheckUserHandler(handler http.Handler) *CheckUser {
	return &CheckUser{handler: handler}
}

func (s *CheckUser) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	 user := context.Get(r, "user").(User)
	 db := context.Get(r, "db").(*mongo.IotDatabase)
	var result User
	err:= db.UserCollection.FindId(user.ID).One(&result)
	if err != nil {
		http.Error(w,"bad", http.StatusNotFound)
	}
	hey, _ := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(hey)
}
