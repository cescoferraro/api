package user

import (
	"encoding/json"
	"net/http"
	"time"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"fmt"
	"github.com/dgrijalva/jwt-go/request"
)

//Authenticate is a type
type Authenticate struct {
	handler     http.Handler
	allowedHost string
}

//AuthenticateHandler is a handler for Authenticate
func AuthenticateHandler(handler http.Handler) *Authenticate {
	return &Authenticate{handler: handler}
}

func (s *Authenticate) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	keyLookupFunc := func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			SendAuthenticateResponse(w, http.StatusUnauthorized, "GO HOME JWT HACKER")
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(viper.GetString("publicKey")), nil
	}

	if token, err := request.ParseFromRequest(r, request.HeaderExtractor{}, keyLookupFunc); err == nil {
		//fmt.Printf("Token for user %v expires %v", token.Claims["user"], token.Claims["exp"])

		if token.Valid {
			SendAuthenticateResponse(w, 200, "VALID JWT")
		} else {
			SendAuthenticateResponse(w, http.StatusUnauthorized, "iNVALID JWT")
		}
	}

}

//SendAuthenticateResponse send an APIResponse encoded in json
func SendAuthenticateResponse(w http.ResponseWriter, status int, reason string) {
	//ResponseAPI is a type
	type LoginResponse struct {
		Time     time.Time
		Success  bool
		Response string
	}

	this := LoginResponse{
		Time:     time.Now(),
		Response: reason,
	}
	hey, _ := json.Marshal(this)
	w.WriteHeader(status)
	w.Write(hey)

}

//e2g7w5f7*x2004
