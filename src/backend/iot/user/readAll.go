package user

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
)

//Status is a type
type All struct {
	handler     http.Handler
	allowedHost string
}

//StatusHandler is a Handler
func AllHandler(handler http.Handler) *All {
	return &All{handler: handler}
}

//StatusResponse is a type
type AllResponse struct {
	Time     time.Time
	Message  string
	Channels map[string]bool
}

func (s *All) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mongo.IotDatabase)
	result := []User{}
	err := db.UserCollection.Find(bson.M{}).All(&result)
	if err != nil {
		SendLoginResponse(w, http.StatusUnauthorized, "User not found!", "")
		return
	}

	type AllResponse struct {
		Time    time.Time
		Success bool
		Users   []User
	}

	this := AllResponse{
		Time:  time.Now(),
		Users: result,
	}
	hey, _ := json.Marshal(this)
	w.WriteHeader(200)
	w.Write(hey)

}
