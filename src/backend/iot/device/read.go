package device

import (
	"encoding/json"
	"net/http"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
	"log"
	"gopkg.in/mgo.v2/bson"
)



//Register is a type
type Read struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func ReadHandler(handler http.Handler) *Read {
	return &Read{handler: handler}
}

func (s *Read) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	house := context.Get(r, "device").(Device)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	var result Device
	log.Println("hellooooo")

		err := db.DeviceCollection.Find(bson.M{"hostname": house.Hostname}).One(&result)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		hey, _ := json.Marshal(result)
		w.WriteHeader(200)
		w.Write(hey)


}







