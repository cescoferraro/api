package device

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
)

//Status is a type
type All struct {
	handler     http.Handler
	AllowedHost string
}

//StatusHandler is a Handler
func AllHandler(handler http.Handler) *All {
	return &All{handler: handler}
}

func (s *All) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mongo.IotDatabase)

	result := []Device{}
	err := db.DeviceCollection.Find(bson.M{}).All(&result)
	if err != nil {
		log.Println(err.Error())
	}

	hey, _ := json.Marshal(result)
	w.WriteHeader(200)
	w.Write(hey)

}
