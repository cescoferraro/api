package device

import (
	"net/http"
	"encoding/json"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
	"log"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
)

//Status is a type
type Logs struct {
	handler      http.Handler
	LogsowedHost string
}


//StatusHandler is a Handler
func LogsHandler(handler http.Handler) *Logs {
	return &Logs{handler: handler}
}


func (s *Logs) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mongo.IotDatabase)

	result := []DeviceLog{}
	err := db.DeviceLogCollection.Find(bson.M{}).Sort("-time").Limit(30).All(&result)
	if err != nil {
		log.Println(err.Error())
	}

	hey, _ := json.Marshal(result)
	w.WriteHeader(200)
	w.Write(hey)

}



