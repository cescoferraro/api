package device

import (
	"gopkg.in/mgo.v2/bson"
	"encoding/json"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"net/http"
	"github.com/gorilla/context"
	"log"
)

type Device struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Hostname string        `json:"hostname"`
	Channels string        `json:"channels"`
	Owner    string    `json:"owner"`
	Alias    string    `json:"alias"`
	Location map[string]string    `json:"location"`
}

type DeviceLog struct {
	ID   bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Time int64    `json:"time"`
	Name string    `json:"name"`
}


//EncodeUserJSON is amiddleware that encodes the body Request into a Users type below
func EncodeDeviceJSON() util.Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/javascript")
			var device Device
			log.Println(r.Body)

			err := json.NewDecoder(r.Body).Decode(&device)
			if err != nil {
				log.Println("ERROO DECODER")
				http.Error(w, "Internal server error.", http.StatusBadRequest)
			} else {
				context.Set(r, "device", device)
				h.ServeHTTP(w, r)
			}
		})
	}
}
