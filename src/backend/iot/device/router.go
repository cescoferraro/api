package device

import (
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/gorilla/mux"
)

func DeviceRoutes(router *mux.Router) *mux.Router {

	Routess := router.PathPrefix("/iot/device").Subrouter()

	Routess.Handle("/all", util.Adapt(AllHandler(router),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectGET(),
		util.EnableCORS(),
	))

	Routess.Handle("/ping", util.Adapt(PingHandler(router),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	Routess.Handle("/logs", util.Adapt(LogsHandler(router),
		mongo.IotMongoDatabaseConnection(),
		util.ExpectGET(),
		util.EnableCORS(),
	))


	Routess.Handle("/read", util.Adapt(ReadHandler(router),
		mongo.IotMongoDatabaseConnection(),
		EncodeDeviceJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))



	Routess.Handle("/update", util.Adapt(UpdateHandler(router),
		mongo.IotMongoDatabaseConnection(),
		EncodeDeviceJSON(),
		util.ExpectPOST(),
		util.EnableCORS(),
	))

	return Routess

}
