package device

import (
	"encoding/json"

	"net/http"

	"log"


	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
	"bitbucket.org/cescoferraro/api/src/backend/iot/ws"
)

//Register is a type
type Update struct {
	handler     http.Handler
	allowedHost string
}

//RegisterHandler is a handler
func UpdateHandler(handler http.Handler) *Update {
	return &Update{handler: handler}
}

func (s *Update) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	device := context.Get(r, "device").(Device)
	db := context.Get(r, "db").(*mongo.IotDatabase)
	var result Device
	log.Println(device.Location["1"])


	change := mgo.Change{
		Update: bson.M{"$set": bson.M{"location": device.Location,"alias":device.Alias}},
		ReturnNew: true,
	}




	info, err := db.DeviceCollection.Find(bson.M{"hostname": device.Hostname}).Apply(change,&result)
	if err != nil {
		log.Println(info)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}


	result.Owner = "dsafds"

	hey, _ := json.Marshal(result)
	ws.Publish("dashboard", "L4@@" + result.Hostname+ "@@" + string(hey))
	w.WriteHeader(200)
	w.Write(hey)
	return
}
