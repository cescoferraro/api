package device

import (
	"net/http"
	"time"
	"encoding/json"
	"log"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
	_ "crypto/sha512"
	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"strconv"
)

//Status is a type
type Ping struct {
	handler     http.Handler
	AllowedHost string
}


//StatusHandler is a Handler
func PingHandler(handler http.Handler) *Ping {
	return &Ping{handler: handler}
}

func (s *Ping) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("ping")
	var preDevice Device
	err := json.NewDecoder(r.Body).Decode(&preDevice)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusBadRequest)
	}
	db := context.Get(r, "db").(*mongo.IotDatabase)

	result := Device{}
	err = db.DeviceCollection.Find(bson.M{"hostname": preDevice.Hostname}).One(&result)
	if err != nil {

		size,_ := strconv.Atoi(preDevice.Channels)
		location := make(map[string]string,size)

		for k := range make([]int,size) {
			location[strconv.Itoa(k+1)]= "undefined"
		}
		device := Device{
			ID:bson.NewObjectId(),
			Hostname:preDevice.Hostname,
			Channels:preDevice.Channels,
			Alias:preDevice.Alias,
			Owner:preDevice.Owner,
			Location:location,
		}
		err = db.DeviceCollection.Insert(device)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

	}

	logg := DeviceLog{
		ID:bson.NewObjectId(),
		Time: time.Now().Unix(),
		Name:preDevice.Hostname,
	}
	err = db.DeviceLogCollection.Insert(logg)
	log.Println(err)
	hey, _ := json.Marshal(logg)
	w.WriteHeader(200)
	w.Write(hey)

}

