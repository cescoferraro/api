package cmd

import (
	"log"

	"bitbucket.org/cescoferraro/api/src/backend/iot/mongo"
	"bitbucket.org/cescoferraro/api/src/backend/iot/user"
	"github.com/Pallinder/go-randomdata"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/bcrypt"
)

var PopulateCommand = &cobra.Command{
	Use:   "populate",
	Short: "A brief description of your command",
	Long:  `A loooooooonger description of your command.`,
	Run: func(cmd *cobra.Command, args []string) {
		done := make(chan bool, 1)
		mongo.IOT_DB.SetSession().SetIotCollections(done)
		<-done

		for i := 0; i < 20; i++ {
			user := new(user.User)
			user.Username = randomdata.SillyName()
			user.Email = randomdata.Email()
			bs, _ := bcrypt.GenerateFromPassword([]byte("cesco"), bcrypt.MinCost)
			user.Password = string(bs)
			if i%2 == 0 {
				user.Role = "master"
			} else {
				user.Role = "user"
			}
			err := mongo.IOT_DB.UserCollection.Insert(user)
			if err != nil {
				log.Println(err.Error())
			}
		}

	},
}

func init() {

	RootCmd.AddCommand(PopulateCommand)
}
