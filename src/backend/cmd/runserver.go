package cmd

import (
	"bitbucket.org/cescoferraro/api/src/backend/iot"

	"fmt"
	"net/http"
	"os"

	"bitbucket.org/cescoferraro/api/src/backend/cats"
	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/fatih/color"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var RunserverCmd = &cobra.Command{
	Use:   "runserver",
	Short: "A brief description of your command",
	Long:  `A loooooooonger description of your command.`,
	Run: func(cmd *cobra.Command, args []string) {
		util.LogIfVerbose(color.FgRed, " APP ", "Bootstraping app ...")
		util.BasicStart()
		iot.RunDependencies()
		cats.ListS3()
		router := InitRootRouter()
		util.LogIfVerbose(color.FgRed, " APP ", "Running the server at 0.0.0.0:"+viper.GetString("port"))
		http.ListenAndServe("0.0.0.0:"+viper.GetString("port"), handlers.CombinedLoggingHandler(os.Stdout, router))

	},
}

func InitRootRouter() *mux.Router {
	router := mux.NewRouter()
	iot_router := iot.InitRouter(router)
	cats_router := cats.InitRouter(iot_router)
	cats_router.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "Welcome to the home page!")
	})
	return cats_router
}

func init() {
	util.CreateShortBoolFlag(RunserverCmd, "VERBOSE", "verbose output", "verbose", "v", util.FlagDefault.VERBOSE)
	util.CreateShortStringFlag(RunserverCmd, "PORT", "Port to run Application server on", "port", "p", util.FlagDefault.PORT)

	util.CreateStringFlag(RunserverCmd, "MONGO_HOST", "MONGO Host Address", "mongo_host", util.FlagDefault.MONGOHOST)
	util.CreateStringFlag(RunserverCmd, "MONGO_PORT", "MONGO Port", "mongo_port", util.FlagDefault.MONGOPORT)

	util.CreateStringFlag(RunserverCmd, "MONGO_USER", "MONGO User", "mongo_user", util.FlagDefault.MONGOUSER)
	util.CreateStringFlag(RunserverCmd, "MONGO_PASS", "MONGO Password", "mongo_pass", util.FlagDefault.MONGOPASS)

	util.CreateStringFlag(RunserverCmd, "REDIS_HOST", "REDIS Host Address", "redis_host", util.FlagDefault.REDISHOST)
	util.CreateStringFlag(RunserverCmd, "REDIS_PORT", "REDIS Port", "redis_port", util.FlagDefault.REDISPORT)
	util.CreateStringFlag(RunserverCmd, "REDIS_PASSWORD", "REDIS Password", "redis_password", util.FlagDefault.REDISPASSWORD)

	util.CreateStringFlag(RunserverCmd, "SCHEDULER_HOST", "SCHEDULER Host Address", "scheduler_host", util.FlagDefault.SCHEDULERHOST)
	util.CreateStringFlag(RunserverCmd, "SCHEDULER_PORT", "SCHEDULER Port", "scheduler_port", util.FlagDefault.SCHEDULERPORT)

	RootCmd.AddCommand(RunserverCmd)
}
