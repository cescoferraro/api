package util

import (
	"net/http"
	"log"
)


//ExpectBody is a middleware
func ExpectBody() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println("The request got a body")
			bodySize := GetBodySize(r)
			if bodySize <= 0 {
				SendAPIResponse(w, http.StatusLengthRequired, "Empty fields")
			} else {
				h.ServeHTTP(w, r)
			}
		})
	}
}

//ExpectPOST is a middleware
func ExpectPOST() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "POST" {
				h.ServeHTTP(w, r)
			} else {
				http.Error(w, "Invalid request method.", http.StatusMethodNotAllowed)

			}

		})
	}
}

//ExpectGET is a middleware
func ExpectGET() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				h.ServeHTTP(w, r)
			} else {
				http.Error(w, "Invalid request method.", http.StatusMethodNotAllowed)
			}

		})
	}
}
//EnableCORS is a middleware
func EnableCORS() Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			if origin := r.Header.Get("Origin"); origin != "" {
				w.Header().Set("Access-Control-Allow-Origin", origin)
				w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
				w.Header().Set("Access-Control-Allow-Headers",
					"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			}
			// Stop here if its Preflighted OPTIONS request
			if r.Method == "OPTIONS" {
				return
			}
			h.ServeHTTP(w, r)
		})
	}
}



