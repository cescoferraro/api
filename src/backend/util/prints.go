package util

import (
	"log"
	"net/http"

	"github.com/fatih/color"

	"sort"

	"github.com/spf13/viper"
)

func LogIfVerbose(cor color.Attribute, block, logg string) {
	if viper.GetBool("verbose") {
		red := color.New(cor).SprintFunc()
		log.Printf("[%s] "+logg, red(block))
	}
}

func RunIfVerbose(logg func()) {
	if viper.GetBool("verbose") {
		logg()
	}
}

var Ciano = color.New(color.FgCyan).Add(color.Underline)

func PanicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func LogIfError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func PrintRequestHeaders(r *http.Request) {
	for k, v := range r.Header {
		log.Println("key:", k, "value:", v)
	}
}

func PrintViperConfig() {
	// TODO: HANDLE NESTED YAMLS BETTER
	keys := viper.AllKeys()
	sort.Strings(keys)
	for _, key := range keys {

		if key == "publickey" || key == "privatekey" {
			LogIfVerbose(color.FgBlue, "VIPER", " "+key+": ******")
		} else {
			LogIfVerbose(color.FgBlue, "VIPER", " "+key+": "+viper.GetString(key))
		}

	}
	return
}
