package util

import (
	"github.com/spf13/viper"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var Secrets = make(map[string]string)

func GetKuberneteSecret(secret ...string) map[string]string {
	kubernete_secret_folder := viper.GetString("kubernete_secret_folder")
	if _, err := os.Stat(kubernete_secret_folder); err == nil {
		filepath.Walk(kubernete_secret_folder, loop)
	}
	return Secrets
}

func loop(path string, f os.FileInfo, err error) error {
	kubernete_secret_folder := viper.GetString("kubernete_secret_folder")
	dir, err := IsDirectory(path)
	if dir == false {
		dat, _ := ioutil.ReadFile(path)
		secret:= strings.Replace(path, kubernete_secret_folder, "", -1)
		Secrets[secret] = strings.Trim(string(dat),string(10))
		data := strings.Replace(secret, "/", ".", -1)
		viper.SetDefault(data, strings.Trim(string(dat),string(10)))
	}

	return err
}

func IsDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	return fileInfo.IsDir(), err
}

//bhjk
