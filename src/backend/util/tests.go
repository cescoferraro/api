package util

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func URL() string {
	return "sdflsdfnd"
}

var Server *httptest.Server
var ServerFlag string

type TableTests struct {
	Method       string
	Path         string
	Body         io.Reader
	BodyContains string
	Status       int
	Name         string
	Description  string
}

func SpinTableTests(server *httptest.Server, t *testing.T, AllTests []TableTests) {
	for _, test := range AllTests {
		log.Println(test.Name, "-", test.Description)
		r, err := http.NewRequest(test.Method, server.URL+test.Path, test.Body)
		assert.NoError(t, err)
		// call handler
		response, err := http.DefaultClient.Do(r)
		assert.NoError(t, err)
		actualBody, err := ioutil.ReadAll(response.Body)
		assert.NoError(t, err)
		//// make assertions
		//log.Println(string(actualBody))
		assert.Contains(t, string(actualBody), test.BodyContains, "body")
		assert.Equal(t, test.Status, response.StatusCode, "status code")
	}
}
