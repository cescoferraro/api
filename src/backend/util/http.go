package util

import (
	"net/http"
	"strconv"

	"github.com/fatih/color"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

type MyRouter struct {
	*mux.Router
}

func GetBodySize(r *http.Request) int {
	bodySize, err := strconv.Atoi(r.Header["Content-Length"][0])
	if err != nil {
		LogIfVerbose(color.FgBlue, "HTTP", "Problem with Content-Lenght")
	}
	return bodySize
}

type Adapter func(http.Handler) http.Handler

// Adapt h with all specified adapters.
func Adapt(h http.Handler, adapters ...Adapter) http.Handler {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

func BasicStart() {
	viper.SetDefault("project_name", "api")
	SetJwtToken()
	GetKuberneteSecret()
	RunIfVerbose(PrintViperConfig)
}
