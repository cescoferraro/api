package util

import (
	"encoding/json"
	"net/http"
	"time"
)

//ResponseAPI is a type
type ResponseAPI struct {
	Time     time.Time
	Response string
}

//SendAPIResponse send an APIResponse encoded in json
func SendAPIResponse(w http.ResponseWriter, status int, reason string) {
	this := ResponseAPI{
		Time:     time.Now(),
		Response: reason,
	}


	hey, _ := json.Marshal(this)
	w.WriteHeader(status)
	w.Write(hey)

}

