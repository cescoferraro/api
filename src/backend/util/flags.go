package util

import (
	"log"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type Flags struct {
	VERBOSE bool
	PORT    string

	PGPORT string
	PGHOST string

	MONGOUSER string
	MONGOPASS string
	MONGOPORT string
	MONGOHOST string

	SCHEDULERPORT string
	SCHEDULERHOST string

	REDISPASSWORD string
	REDISPORT     string
	REDISHOST     string
}

var FlagDefault = Flags{
	VERBOSE: true,
	PORT:    "9000",

	MONGOUSER: "admin",
	MONGOPASS: "descriptor8",

	MONGOHOST: setOSdefault("mongo.db.svc.cluster.local", "api_mongo_1", "MONGO_PORT_27017_TCP_ADDR"),
	MONGOPORT: "27017",

	SCHEDULERHOST: setOSdefault("scheduler.db.svc.cluster.local", "api_kala_1", "SCHEDULER_PORT_8000_TCP_ADDR"),
	SCHEDULERPORT: "8000",

	REDISHOST:     setOSdefault("redis.db.svc.cluster.local", "api_redis_1", "REDIS_PORT_6379_TCP_ADDR"),
	REDISPORT:     "6379",
	REDISPASSWORD: "descriptor8",
}

var pristine = 1

func PRISTINELOGGGER(block, description string) {
	if pristine == 1 {
		log.Println("[", block, "] ", description)
	}
}

func setOSdefault(kube, docker, wercker string) string {
	// TODO: Implement a switch case ans log the platform
	if os.Getenv("KUBERNETES") == "true" {
		PRISTINELOGGGER("APP", "Inside Kubernetes Cluster")
		pristine = 2
		return kube
	}
	if os.Getenv("WERCKER") == "true" {
		PRISTINELOGGGER("APP", "Inside Wercker CI")
		pristine = 2
		return os.Getenv(wercker)
	}

	if _, err := os.Stat("/.dockerinit"); err == nil {
		PRISTINELOGGGER("APP", "Inside Docker Container")
		pristine = 2
		return docker
	}
	if runtime.GOOS == "darwin" {
		PRISTINELOGGGER("APP", "Inside OSX")
		pristine = 2
		return "192.168.99.100"
	}

	PRISTINELOGGGER("APP", "Inside Linux Box")
	pristine = 2
	return "127.0.0.1"

}

func CreateStringFlag(cmd *cobra.Command, envname, description, name, thisdefault string) {
	viper.BindEnv(envname)
	cmd.Flags().String(name, thisdefault, description)
	viper.BindPFlag(envname, cmd.Flags().Lookup(name))
}

func CreateShortStringFlag(cmd *cobra.Command, envname, description, name, short, thisdefault string) {
	viper.BindEnv(envname)
	cmd.Flags().StringP(name, short, thisdefault, description)
	viper.BindPFlag(envname, cmd.Flags().Lookup(name))
}

func CreateShortBoolFlag(cmd *cobra.Command, envname, description, name, short string, thisdefault bool) {
	viper.BindEnv(envname)
	cmd.Flags().BoolP(name, short, thisdefault, description)
	viper.BindPFlag(envname, cmd.Flags().Lookup(name))
}
