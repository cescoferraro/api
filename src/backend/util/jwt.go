package util

import (
	"github.com/spf13/viper"
)

func SetJwtToken(){
	//filename, _:= os.Getwd()
	//privateKey, _ := ioutil.ReadFile(path.Join(filename, "extras/token.rsa"))
	//publicKey, _ := ioutil.ReadFile(path.Join(filename, "extras/token.rsa.pub"))

	ddprivateKey := `-----BEGIN RSA PRIVATE KEY-----
	MIICXAIBAAKBgQCSv5ajEXfbb9/zT3snkBAchCPD925QzWGIMwYdEvLi2Frs8khn
	kpIJEK4oFLEa8Z4d0mdtqeCewgMjaiLHyOm0rO/KCUHh9HwglXVYl3+hiLoo1CiM
	zNbitFf3KgUkI4Z6DY7H7elJes0ssRUmfaSsa9xZxJJ8Y3Oiexb1bhn8BwIDAQAB
	AoGAALLYzL3wjCLkNgxS0cVlzjzyzqQG6muP404a4ViNv3a4OBiPL68K8vAZZCd1
	EGPGFO5Df8onP5o82ZZPxBO4yRmRNtgn9WNttVnme9LQpYbC0Q2Fi+BXqXfvpn/s
	HUCMb8W1hsCbOfpWijzmdtH4/FofWiUoVT2UTWvQ8SvyhgECQQDC6hA8GwA2DuO7
	bvVCrbICuXm5J6LpS4QEeqawC/18I02t1ZarHiSAIV5TqUWfj2zfhw4cJTyLyx6K
	yGDlXYThAkEAwL0u1LK+JOlQ3XFIAmIQ1H2OAeWCmG+gqGHhPMBkeA2To5S2Ezid
	IaF8xd76w/xd0X++HIwaK7m+6xX5jES15wJAIGEPt1xo6ZIJpM5m8d+27ZrwLKD4
	ADdqQ/A4FpC3WFLpCZKsx9pnB94DNHSvOzVxSaS+5FAPQc3JUha9efzSIQJAA5yH
	4BxpS6/DgCK7QrAruI+RtZ9jTw/oZr9OxSClwUof1hTXRIzvBM5N1DdVY467A03t
	dgN6Cg+08Bq9Tk+LSQJBAJhgpwN4SgbwAUWwc0pRTJuasGJRSJWA7bza+JkYQXuF
	J2SsuRtI3znYIPx5NIM4hPZExCXCcVGEecWetni1OV4=
	-----END RSA PRIVATE KEY-----`


	ddpublicKey := `-----BEGIN PUBLIC KEY-----
	MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCSv5ajEXfbb9/zT3snkBAchCPD
	925QzWGIMwYdEvLi2Frs8khnkpIJEK4oFLEa8Z4d0mdtqeCewgMjaiLHyOm0rO/K
	CUHh9HwglXVYl3+hiLoo1CiMzNbitFf3KgUkI4Z6DY7H7elJes0ssRUmfaSsa9xZ
	xJJ8Y3Oiexb1bhn8BwIDAQAB
	-----END PUBLIC KEY-----`



	viper.SetDefault("privateKey", ddprivateKey)
	viper.SetDefault("publicKey", ddpublicKey)
}
