package cats

import (
	"bytes"
	"fmt"
	"image/jpeg"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"

	"bitbucket.org/cescoferraro/api/src/backend/util"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/fatih/color"
	"github.com/nfnt/resize"
)

var CatsLogsColor = color.FgHiWhite
var CatsLogsBlock = "CATS "

//Cat is a type
type Cat struct {
	handler     http.Handler
	allowedHost string
}

//CatHandler is a Cat Handler
func CatHandler(handler http.Handler) *Cat {
	return &Cat{handler: handler}
}

var allPhotos []s3.Object

func AWSSession() (*session.Session, error) {

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Creating credentials ...")
	acess := "AKIAJW3EWJ4GLDOZ7HCA"
	secret := "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"

	creds := credentials.NewStaticCredentials(acess, secret, "")
	_, err := creds.Get()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Creating session ...")
	sess, err := session.NewSession(&aws.Config{
		Credentials: creds,
		Region:      aws.String("sa-east-1"),
		//LogLevel:    aws.LogLevel(aws.LogDebug),
	})
	return sess, err
}

func ListS3() {
	done := make(chan bool, 1)

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Gathering Dependencies ...")
	sess, err := AWSSession()

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Creating S3 ...")
	MYS3 := s3.New(sess)
	params := &s3.ListObjectsInput{
		Bucket: aws.String("cescocats"),
	}

	resp, err := MYS3.ListObjects(params)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//log.Println(resp.String())
	for _, key := range resp.Contents {
		allPhotos = append(allPhotos, *key)
		util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, *key.Key)
	}

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Finishing Dependencies ...")
	done <- true
}

func (s *Cat) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	width := "400"
	if r.FormValue("w") != "" {
		util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "tem width query")
		width = r.FormValue("w")
	}

	sess, err := AWSSession()
	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Creating S3Manager ...")
	manager := s3manager.NewDownloader(sess, func(d *s3manager.Downloader) {
		d.PartSize = 1 * 1024 * 1024 // 64MB per part
	})

	var buffer []byte
	var awsbuffer = aws.NewWriteAtBuffer(buffer)
	parametros := &s3.GetObjectInput{
		Bucket: aws.String("cescocats"),
		Key:    allPhotos[rand.Intn(len(allPhotos))].Key}

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Downloading image ...")
	_, err = manager.Download(awsbuffer, parametros)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	awsfile := bytes.NewReader(awsbuffer.Bytes())

	util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "Deconding jpg image ...")
	// decode jpeg into image.Image
	img, err := jpeg.Decode(awsfile)
	if err != nil {
		log.Fatal(err)
	}

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	finalwidth, _ := strconv.Atoi(width)
	m := resize.Resize(uint(finalwidth), 0, img, resize.Lanczos3)

	thisbuffer := new(bytes.Buffer)

	if err := jpeg.Encode(thisbuffer, m, nil); err != nil {
		util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "unable to encode image.")
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(thisbuffer.Bytes())))
	if _, err := w.Write(thisbuffer.Bytes()); err != nil {
		util.LogIfVerbose(CatsLogsColor, CatsLogsBlock, "unable to write image.")
	}

}
