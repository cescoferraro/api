package cats

import (
	"github.com/gorilla/mux"
	"bitbucket.org/cescoferraro/api/src/backend/util"
)



func InitRouter(router *mux.Router) *mux.Router {
	router.Handle("/cats", util.Adapt(CatHandler(router)))
	return router
}
