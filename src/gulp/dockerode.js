import gulp from 'gulp';
import util from 'gulp-util';
import conf from './conf';
import Docker from 'dockerode';
import tar from 'tar-fs';
import fs from 'fs';

var docker = new Docker();


gulp.task('dockerode:build', (callback) => {
	docker.buildImage(tar.pack(conf.paths.base), {t: conf.imageNameBase}, (error, stream) => {
			if (error) return;
			var logFile = fs.createWriteStream('/dev/null', { flags: 'a' });
			stream.pipe(logFile, {end: true});
			stream.on('end',  () => {
				callback()
			})
		}
	)
});





gulp.task('dockerode:run', (callback) => {
	let options = {
		Image: conf.imageNameBase,
		Tty: true,
		Rm:true,
		ExposedPorts: {'9000/tcp': {}},
		HostConfig: {PortBindings: {'9000/tcp': [{HostPort: "9000"}]}}
	}
	docker.createContainer(options,
		(err, container) =>{
			util.log(err)
			container.start( (err, data)=> {
				if (err) util.log(err)


				process.once('SIGINT', () => {
					container.stop(function (err, data) {
					});
				});
				container.attach({stream: true, stdout: true, stderr: true},
					function (err, stream) {
						stream.pipe(process.stdout,{end:false});

					}
				);
			});
		}
	);
});
