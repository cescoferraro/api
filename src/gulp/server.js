import gulp from 'gulp';
import child from 'child_process';
import util from 'gulp-util';
import notifier from 'node-notifier';
import reload from 'gulp-livereload';
import conf  from './conf.js';
var argv = require('yargs').argv;
var server = null;



gulp.task('server:build', function () {
	var build;
	var output = argv.prod ? conf.paths.build + '/prod/bin' : conf.paths.build + '/dev/bin';

	let options = {
		env: {
			'PATH': process.env.PATH,
			'GOPATH': process.env.GOPATH
		}
	}

	if (argv.prod) {
		console.log("Compiling go binarie to run inside Docker container")
		options.env['GOOS'] = 'linux'
		options.env['CGO_ENABLED'] = '0'
	}

	build = child.spawnSync('go', ['build', '-o', output, "src/backend/main.go"], options, { shell: true, stdio: 'inherit' });
	if (build.stderr.length) {
		var lines = build.stderr.toString()
			.split('\n').filter(function (line) {
				return line.length
			});
		for (var l in lines)
			util.log(util.colors.red(
				'Error (go install): ' + lines[l]
			));
		notifier.notify({
			title: 'Error (go install)',
			message: lines
		});
	}
	return build;
});

gulp.task('server:spawn', function () {

	var output = argv.prod ? conf.paths.base + 'dist/prod/bin' : conf.paths.base + 'dist/dev/bin';
	var output_base = argv.prod ? conf.paths.base + 'dist/prod' : conf.paths.base + 'dist/dev';


	if (server)
		server.kill();

	server = child.spawn(output,
		['runserver'], { shell: true, stdio: 'inherit' } , function (error, stdout, stderr) {
			// work with result
		});

	// server.stdout.once('data', function () {
	// 	reload.reload('/');
	// });

	// /* Pretty print server log output */
	// server.stdout.on('data', function (data) {
	// 	var lines = data.toString().split('\n');
	// 	for (var l in lines)
	// 		if (lines[l].length)
	// 			util.log(lines[l]);
	// });

	// /* Print errors to stdout */
	// server.stderr.on('data', function (data) {
	// 	process.stdout.write(data.toString());
	// });
});



gulp.task('server:reload', function () {
	reload.reload('/');
});

