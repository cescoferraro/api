import path from 'path';


let base = path.join(__dirname, '../../');

export default {
	api: {
		binaryName: 'web',
		packageName: 'bitbucket.org/cescoferraro/api',
		devServerPort: 9000
	},
	imageNameBase:  'cescoferraro/api:latest',
	paths: {
		base: base,
		build: "dist",
		allgo: path.join(base, './src/backend/**/*.go')

	},
	defaultTest: './src/test/...'

}



