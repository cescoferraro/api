import gulp from 'gulp';
import reload from 'gulp-livereload';
import sequence from 'gulp-sequence';
import del from 'del';
import conf from './conf';
var argv = require('yargs').argv;


gulp.task('clean', function () {
	if (argv.dev == true) {
		return del(['dist/dev/']);
	}
	if (argv.prod == true) {
		return del(['dist/prod/']);
	}
	return del(['dist/']);
});



gulp.task('default',  function (cb) {
	sequence('clean','copy','server:build', 'server:spawn','watch')(cb)
});


gulp.task('watch', function () {
	gulp.watch(conf.paths.allgo,
		['server:build',
			'server:spawn',
			'server:reload'
		]);
});
