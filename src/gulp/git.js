import gulp from 'gulp';
import git from 'gulp-git';
import conf from './conf';
import sequence from 'gulp-sequence';

// Run git add
// src is the file(s) to add (or ./*)
gulp.task('add', function(){
  return gulp.src(conf.paths.base)
    .pipe(git.add());
});

gulp.task('commit', function(){
  return gulp.src(conf.paths.base)
    .pipe(git.commit(new Date().toTimeString()));
});


gulp.task('push', function(callback){
  git.push('origin', ['master'],  function (err) {
    if (err) throw err;
	callback()
  });
});


gulp.task('git',   (callback) => {
	sequence('add', 'commit', 'push')(callback)
});
