import gulp from 'gulp';
import sequence from 'gulp-sequence';
import conf from './conf';
var argv = require('yargs').argv;



gulp.task('copy', function (callback) {
	sequence('copy:extras')(callback)
});



gulp.task('copy:extras', function () {
	let output = argv.prod ? 'dist/prod/extras': 'dist/dev/extras';
	return gulp.src([conf.paths.base + 'src/extras/**/*'], {base: './src/extras'})
		.pipe(gulp.dest(output));
});
