import gulp from 'gulp';
import child  from 'child_process'
import util from 'gulp-util';
import sequence from 'gulp-sequence';
import conf from "./conf";



function execCommand (command, args, cb)  {
	const ctx = child.spawn(command, args, { shell: true, stdio: 'inherit' });
	ctx.on('close', (code) => {
		if(cb){cb(code === 0 ? null : code);}
	})
}


gulp.task('test', (cb) => {
	execCommand('go', ['test', '-v', conf.defaultTest], cb);
});

