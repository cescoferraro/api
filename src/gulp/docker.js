import gulp from 'gulp';
import child  from 'child_process'
import util from 'gulp-util';
import conf from './conf';
import sequence from 'gulp-sequence';



function execCommand(command, args, cb)  {
	const ctx = child.spawn(command, args);
	ctx.stdout.on('data', (data) => {
		process.stdout.write(data);
	});
	ctx.stderr.on('data', (data) => {
		process.stderr.write(data);
	});
	ctx.on('close', (code) => {
		if(cb){cb(code === 0 ? null : code);}
	})
}

gulp.task('docker:push', (cb) => {
    execCommand('docker', ['push', conf.imageNameBase], cb);
});
gulp.task('docker:run', (cb) => {
    execCommand('docker', ['run', '--rm' ,'-p','9000:9000',conf.imageNameBase], cb);
});

gulp.task('docker:build', (cb) => {
    execCommand('docker',['build', '-t', conf.imageNameBase, '.'], cb);
});

gulp.task('docker:compose', (cb) => {
    execCommand('docker-compose',['up', '-d', 'mongo', 'redis'], cb);
});

gulp.task('docker:package', (cb) => {
	sequence('docker:build', 'docker:push')(cb)
});
