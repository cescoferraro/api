FROM progrium/busybox
RUN  opkg-install ca-certificates ntpd

RUN mkdir /secrets/
ADD ./dist/prod /srv/app
WORKDIR /srv/app
CMD ["./bin", "runserver"]
EXPOSE 9000
